﻿# CMakeList.txt: klee 的 CMake 项目，在此处包括源代码并定义
# 项目特定的逻辑。
#
cmake_minimum_required (VERSION 3.8)

project (klee VERSION 1.0 LANGUAGES CXX)


set(CMAKE_WINDOWS_EXPORT_ALL_SYMBOLS ON)
set(CMAKE_CXX_STANDARD 20)
set(CMAKE_CXX_STANDARD_REQUIRED ON)

#最后引用子文件夹里的CMakeLists.txt，提供一个选项是否编译Test

option(BUILD_K1EE_TESTS "Build Tests" ON)

include(GNUInstallDirs)

add_subdirectory(src)
add_subdirectory(include)

if(BUILD_K1EE_TESTS)
    enable_testing()
    add_subdirectory(test)
endif()
