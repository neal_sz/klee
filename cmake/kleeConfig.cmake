﻿# CMakeList.txt: klee 的 CMake 项目，在此处包括源代码并定义
# 项目特定的逻辑。
#
@PACKAGE_INIT@

include("${CMAKE_CURRENT_LIST_DIR}/kleeTargets.cmake")
check_required_components("@PROJECT_NAME@")

# TODO: 如有需要，请添加测试并安装目标。
